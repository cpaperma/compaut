# coding: utf-8
import compAut as ca
from compAut.metas import *

def all_bytes(n):
	if n==0:
		return [""]
	B = all_bytes(n-1)
	return ["0"+u for u in B]+["1"+u for u in B]

B = all_bytes(8)

E_1 = allIn(e for e in B if e[:3] == "110") # To correct
E_2 = allIn(e for e in B if e[:4] == "1110") # To correct
E_3 = allIn(e for e in B if e[:5] == "11110") # To crrect
Continuation = allIn(e for e in B if e[:2] == "10")
tr = [
    (0, allIn(e for e in B if e[0] == "0"), 0),
    (0, allIn("11100000"), "1_next_3_continuation"),
    (0, allIn("11101101"), "2_surrogate"),
    (0, allIn("11110000"), "overlong"),
    (0, allIn(E_1), "one_more"),
    (0, E_2, "two_more"),
    (0, E_3, "three_more"),
    (0, allIn("11110100"), "too_large"),
    ("1_next_3_continuation", allIn(e for e in B if e[:3] == "101"), "one_more"),
    ("2_surrogate", allIn(e for e in B if e[:3] == "100"), "one_more"),
    ("overlong", allIn(e for e in B if e[:3] == "101" or e[:4] == "1001"), "two_more"),
    ("one_more", Continuation, 0),
    ("two_more", Continuation, "one_more"),
    ("three_more", Continuation, "two_more"),
    ("too_large", allIn(e for e in B if e[:4] == "1000"), "two_more")
]

UnicodeByte = ca.models.Automaton.from_transitions(tr, initial_states=[0], final_states=[0])
"""
	The following table is extracted to the wikipedia page
	https://fr.wikipedia.org/wiki/UTF-8
"""
desc = """0xxxxxxx
110xxxxx10xxxxxx
11100000101xxxxx10xxxxxx
1110000110xxxxxx10xxxxxx
1110001x10xxxxxx10xxxxxx
111001xx10xxxxxx10xxxxxx
111010xx10xxxxxx10xxxxxx
1110110010xxxxxx10xxxxxx
11101101100xxxxx10xxxxxx
1110111x10xxxxxx10xxxxxx
111100001001xxxx10xxxxxx10xxxxxx
11110000101xxxxx10xxxxxx10xxxxxx
1111000110xxxxxx10xxxxxx10xxxxxx
1111001x10xxxxxx10xxxxxx10xxxxxx
111101001000xxxx10xxxxxx10xxxxxx"""

tr = []
lines = 0
for i, l in enumerate(desc.split("\n")):
	n = len(l)
	for j, letter in enumerate(l):
		if j:
			start = (i, j)
			end = (i, j+1) if j != n -1 else (0, 0)
		else:
			start = (0, 0)
			end = (i, 1)
		if letter == "x":
			letter = "01"
		tr.append((start, allIn(letter), end))

Unicode1Bit = ca.models.Automaton.from_transitions(tr, initial_states=[(0, 0)], final_states=[(0, 0)])

def expand(u):
	if not u:
		return [""]
	d = expand(u[1:])
	if u[0] == "x":
		return ["0"+x for x in d] + ["1"+x for x in d]
	return [u[0]+x for x in d]

tr = []
lines = 0
for i, l in enumerate(desc.split("\n")):
	n = len(l)
	for j, letter in enumerate(l):
		if j%4 != 0:
			continue
		if j:
			start = (i, j)
			end = (i, j+4) if j != n -4 else (0, 0)
		else:
			start = (0, 0)
			end = (i, 4)
		u = l[j:j+4]
		u = expand(u)
		tr.append((start, allIn(u), end))

Unicode4Bit = ca.models.Automaton.from_transitions(tr, initial_states=[(0, 0)], final_states=[(0, 0)]).minimize()

tr = []
lines = 0
for i, l in enumerate(desc.split("\n")):
	n = len(l)
	for j, letter in enumerate(l):
		if j%8 != 0:
			continue
		if j:
			start = (i, j)
			end = (i, j+8) if j != n - 8 else (0, 0)
		else:
			start = (0, 0)
			end = (i, 8)
		u = l[j:j+8]
		u = expand(u)
		tr.append((start, allIn(u), end))

Unicode8bit = ca.models.Automaton.from_transitions(tr, initial_states=[(0, 0)], final_states=[(0, 0)]).minimize()
