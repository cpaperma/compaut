#TODO: improve that with pygments later

class code_repr:
	def __init__(self, content, language=None):
		self.content = content
		self.language = language
		try:
			import pygments as pygs
			import pygments.formatters.terminal
			self.html_formatter = pygs.formatters.HtmlFormatter()
			self.terminal_formatter = pygs.formatters.terminal.TerminalFormatter()
			self.lexer = pygs.lexers.get_lexer_by_name(language)
			self.highlight = pygs.highlight
		except ModuleNotFoundError:
			self.html_formatter = None 
			self.terminal_formatter = None 
			self.lexer = None
			self.highlight = lambda code, lexer, formatter:code
		
	def __repr__(self):
		return self.highlight(self.content, self.lexer, self.terminal_formatter)

	def __repr_html_(self):
		return self.highlight(self.content, self.lexer, self.html_formatter)

	def __str__(self):
		return self.content
