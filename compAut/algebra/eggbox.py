import tempfile
from compAut.utils import show_img

class eggbox:
	def __init__(self, semigroup):
		self.semigroup = semigroup
		semigroup.precompute_greens_relation()
		self.boxes = {d: self.compute_box(d) for d in self.semigroup.D_classes}

	def compute_box(self, element):
		"""
		For each D-class compute the box diagram.
		Try to put idempotents on the diagonal.
		"""
		S = self.semigroup
		D = S.D_class_of(element)
		R = D.intersection(S.R_classes)
		L = D.intersection(S.L_classes)
		idempotents = D.intersection(S.idempotents)
		if len(idempotents) == 0:
			R = list(R)
			L = list(L)
		else:
			R = sorted(R, key=lambda e:len(S.R_class_of(e).intersection(idempotents)))
			L = sorted(L, key=lambda e:len(S.L_class_of(e).intersection(idempotents)))
			# TODO: improve that
		return [[ self.semigroup.L_class_of(x).intersection(S.R_class_of(y)) for x in L] for y in R]

	def graphviz_graph(self, repr_el=None):
		try:
			import pygraphviz as pyg
		except ModuleNotFoundError:
			raise ModuleNotFoundError("Drawing an automaton requires the module pygraphviz. Install it using `pip3 install -U pygraphviz`")
		S = self.semigroup
		G = pyg.AGraph(directed=True)
		G.node_attr["shape"] = "record"
		representents = set(self.boxes.keys())
		for i, M in S._D.nodes("members"):
			x = next(iter(representents.intersection(M)))
			box = self.boxes[x]
			G.add_node(i, label=self.graphviz_box(x, repr_el=repr_el))
		G.add_edges_from(S._D.edges())
		return G

	def graphviz_box(self, element, repr_el=None):
		box = self.boxes[element]
		rotate_box = []
		S = self.semigroup
		if repr_el is None:
			repr_el = lambda el: f"{el}" if S(el, el) != el else f"{el}*"
		else:
			if repr_el == "normalize":
				rep = list(S)
				rep = {e:i for i,e in enumerate(rep)}
				repr_el = lambda el: f"{rep[el]}" if S(el, el) != el else f"{rep[el]}*"

		def repr_H(H):
			return ", ".join(map(repr_el, H))

		def repr_R(R):
			return "{" + "|".join(map(repr_H, R)) + "}"
		return  "|".join(map(repr_R, box)) 

	def draw_graphviz(self, filepath, repr_el=None):
		G = self.graphviz_graph(repr_el=repr_el)
		G.layout(prog="dot")
		G.draw(filepath)


	def show_graphviz(self, savefig=None, repr_el=None):
		if savefig is None:
			fd, path = tempfile.mkstemp(suffix=".png")
			open(fd).close()
		else:
			path = savefig
		self.draw_graphviz(path, repr_el=repr_el)
		return show_img(path)

