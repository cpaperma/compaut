from .semigroups import Semigroup, FiniteSemigroup

class Monoid(Semigroup):
	def __init__(self, elements, product=None, identity=None, **kwargs):
		super().__init__(elements, product=product, **kwargs)
		if identity is None:
			if "identity" in dir(elements):
				identity = elements.identity
			if identity is None and "identity" in dir(product):
				identity = product.identity
		self.identity = identity

	@classmethod
	def from_semigroup(cls, semigroup):
		m, i = semigroup.add_identity()
		return cls(m, m, i)

	def identity(self):
		return self.identity

	def __call__(self, *other):
		if len(other) == 0:
			return self.identity
		return super().__call__(*other)

	def quotient(self, quotient_map):
		res = super().quotient(quotient_map)
		res.identity = quotient_map(self.identity)

	def cartesian_product(self, other):
		res = super().cartesian_product(other)
		res.identity = (self.identity, other.identity)
		return res

	def submonoid_generated_by(self, elements):
		if "__iter__" not in dir(elements):
			elements = (elements,)
		if self.identity not in elements:
			elements = list(elements)
			elements.append(self.identity)
		return self.generated_by(elements, self.product, identity=self.identity)

	def power(self, x, n):
		if n==0:
			return self.identity
		else:
			return super().power(x, n)

class FiniteMonoid(Monoid, FiniteSemigroup):
	pass

