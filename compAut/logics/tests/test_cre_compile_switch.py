from compAut.logics import cre
from compAut import machines
import pytest

def test_compile_SwitchGrepLike_blogpost():
	"""
	Following examples extracted (and freely adapted) from :
	https://code.tutsplus.com/tutorials/8-regular-expressions-you-should-know--net-6149
	"""
	sw = machines.SwitchGrepLikeAutomaton
	m1 = sw(cre.compile("[a-z0-9_\-]{3,16}"), name="user_name")
	examples = "\n".join(("my-us3r_n4m3", "th1s1s-wayt00_l0ngt0beausername"))
	m1.compile()
	output = m1.execute_on_str(examples)
	m1.clean()

	assert output.strip() == "my-us3r_n4m3"

	m2 = sw(cre.compile("[a-z0-9_\-]{6,18}"), name="password")
	m2.compile()
	examples = "\n".join(("myp4ssw0rd", "mypa$$w0rd "))
	output = m2.execute_on_str(examples)
	assert output.strip() == "myp4ssw0rd"
	m2.clean()

	m3 = sw(cre.compile("#([A-F0-9]{6}|[a-f0-9]{6})"), name="hexa")
	m3.compile()
	examples = "\n".join((
	"#a3c113",
	"#A3C113",
	"#a3C113",
	"#a3G113",
	"#a3C1130",
	))
	output = m3.execute_on_str(examples)
	assert tuple(map(str.strip, output.split())) == ("#a3c113", "#A3C113")
	m3.clean()

	m4 = sw(cre.compile("([a-z0-9_\.\-]+)@([a-z\.\-]+)\.([a-z\.]{2,6})"), name="mail")
	m4.compile()
	examples = "\n".join(("john@doe.com", "john@doe.something", "johndoe.com"))
	output = m4.execute_on_str(examples)
	assert output.strip() == "john@doe.com"
	m4.clean()
