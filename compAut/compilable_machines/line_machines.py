from compAut.compile import TemplateCompiler


class SimpleLineMachine(TemplateCompiler):
	"""
	A Compilable Machine is expected to produce C-code through
	its crun methods. This class will produce the non-optimized code to interface
	with the template.
	It will read the stream line by line and will apply transitions with a switch statement
	over each lines.


	To implement a SimpleLineMachine you need to provides:
		* a header to initialize globally the machine
		* a reinint to reinitialize the machine at the begining of each line
		* a main_loop to execute on each char of the line.
		* an accepting operation to run at the end of each line
	"""
	name = "LineMachine"
	descr = "Abstract (not executable) machine that read input line by line using `getline` from `stdio.h`."

	def header(self):
		return ""

	def reinit(self):
		return ""

	def accepting(self):
		return ""

	def main_loop(self):
		return ""

	@property
	def initilization(self):
		return f"""
	char * line=NULL;
	char c;
	size_t n = 0;
	ssize_t r;
	int i=0;
	int state;
	{self.header()}
	"""

	def crun(self):
		"""
		Produces a valid crun C-code block assuming
		header and switch are well-defined.
		In the following:
			f is a FILE (stream)
			directives are not used
			outputs is a FILE (stream) to store outputs.
		"""
		return f"""
	{self.initilization}
	for (;;){{
		{self.reinit()}
		r = getline(&line, &n, f);
		if (r == -1) break;
		for (i=0;i<n;i++){{
			c = line[i];
			if (c==0) break;
			{self.main_loop()}
		}}
		if ({self.accepting()}) fprintf(outputs, "%s", line);
	}}
	"""

