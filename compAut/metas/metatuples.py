from compAut.metas import allExcept, metaLetter, allIn
from compAut.utils import shorten
import itertools

class metaObject:
	pass

class metaTuple(metaObject):
	"""
	A class of tuples of metaletters that support intersection, union, complement.
	It also support equality between some of the component.

	A metaTuple is hashable and non-mutable.

	* metaTuple creation:
		T = metaTuple(("ab", range(3), allExcept("de"), allExcept("cd")), equals=[(2,3)])
	T is the metaTuple: (allIn({"a", "b"}), allIn({0, 1, 2}), AlExcept({"c", "d", "e"}), allExcept({"c", "d", "e"}))
	together with the constraint that the two last values must to be always equals.
	("a", 0, "g", "g") in T but
	("a", 0, "g", "h") not in T.

		T = metaTuple.identity(d) # will create the metaTuple metaTuple((allExcept(),)*d, equals=range(d))

	Equality conditions can be also obtained from metaTuple transformation:

		K = T.equals(0, 1) make the first and second component equals in K.
		K = T.different(0, 1) make the first and second component always different in K.

		K = T.component_intersection(allIn("abcd"), 3) will intersect the 3 component with the provided
	metaLetter (or iterable cast to allIn automatically).

	Remark that unions of metaTuple is not necessary a metaTuple.
	For instance (allIn("a"), allExcept("a")) union (allExcept("a"), allIn("a")) is not a metaTuple.
	However intersection of metaTuple is a metaTuple.
	Because of that, union and complement will return metaTuples object and not metaTuple object.


	Internal representation:
		Although metaTuple are non mutable and hashable, the equality and inequality constraints
		are stored through two dict _equals and _not_equals providing for each component the set of component
		it is equals to.

	Automatic simplification:
		If two dimensions must be equals and different simultaneously (non-satisfiable constraint), the metaTuple
		will be automatically simplified to (allIn(),)*d with d its dimension.

		Similarly, if two dimension must be equals and belongs to two disjoint set, the metaTuple will be simplified.
		More generally, metaTuple's dimension that must be equals will have the same metaLetter.

	Interesting properties:
		Because of the simplification, metaTuple enjoy the property that their projection over
		one component is actually surjective.

	Precise semantics of underlying data-structure:
		* self._equals dict indices => set of indices. The set is shared between indices in the set.
			If j in self._equals[i] then self._equals[i] is self._equals[j].
			We have transitive closure:
				 If i in self._equals[j] and j in self._equals[k] then i in self._equals[k].
			All indices sharing a set must be equals to the same values.

		* self._not_equals: dict indices => set of indices.
			If j in self._not_equals[i] then i in self._not_equals[j].
			The underlying relation is *not* transitive (since i != k, k!= j but i=j is possible).
			If i in self._equals[j] and j in self._not_equals[k] then i in self._not_equals[k].

	"""

	def __new__(cls, metatuple, equals=(), not_equals=(), dimension=None):
		self = object.__new__(cls)
		metatuple = list(allIn(e) if not isinstance(e, metaLetter) else e for e in metatuple)
		if dimension is not None:
			metatuple.extend((allExcept(),)*(dimension - len(metatuple)))
		self.dimension = dimension = len(metatuple)
		self._equals = {i:set([i]) for i in range(dimension)}
		id_set = {id(d):d for d in self._equals.values()} #Retrieve set from their ids.
		for i, j in equals:
			Si = self._equals[i]
			Si.update(self._equals[j])
			self._equals[j] = Si # two equivalent indice must share the same set.
		self._uniq_set = uniq_set = [id_set[i] for i in set(map(id, self._equals.values()))] #get uniq set, partitionning range(dimension)
		for S in uniq_set:
			k = tuple(S)
			m = metatuple[k[0]]
			m = m.intersection(*(metatuple[i] for i in k[1:]))
			for i in k:
				metatuple[i] = m # We take the intersection of all tuple that must be empty.
		self._not_equals = {i:set([]) for i in range(dimension)}
		for i, j in not_equals:
			self._not_equals[i].update(self._equals[j])
			self._not_equals[j].update(self._equals[i])
		for i in range(dimension):
			if i in self._not_equals[i]:
				return metaTuple.empty_tuple(dimension)
		if not all(metatuple):
			metatuple = tuple(allIn() for i in range(dimension))
			self._equals = {}
			self._not_equals = {}
			self._uniq_set = []
			#If one dimension is empty, the whole is empty.
		# Remove equals condition for allIn set of size 1.
		for S in tuple(self._uniq_set):
			i = next(iter(S))
			m = metatuple[i]
			if isinstance(m, allIn) and len(m) == 1:
				for j in S:
					self._equals[j] = set()
				self._uniq_set.remove(S)
		# Remove inequality between allIn of size 1 and something else.
		for i in range(dimension):
			m = metatuple[i]
			if not (isinstance(m, allIn) and len(m) == 1):
				continue
			for j in self._not_equals[i]:
				metatuple[j] = metatuple[j].difference(m)
			self._not_equals[i] = {}
		self._metatuple = tuple(metatuple)
		return self

	def __repr__(self):
		if self.dimension == 1:
			return repr(self[0])
		eq = shorten(", ".join(f"{i}={j}" for i,j in self.equals_iter() if i<j))
		neq = shorten(", ".join(f"{i}≠{j}" for i,j in self.not_equals_iter() if i<j))
		extra = ""
		if eq:
			extra += f";{eq}"
		if neq:
			extra += f";{neq}"
		return f"{'×'.join(e.__repr__() for e in self._metatuple)}{extra}"

	@classmethod
	def empty_tuple(cls, dimension):
		return cls((allIn(),)*dimension)

	@classmethod
	def identity(cls, dimension, carrying_set=allExcept()):
		if not isinstance(carrying_set, metaLetter):
			carrying_set = allIn(carrying_set)
		return cls((carrying_set,)*dimension, equals=enumerate(range(1, dimension)))

	@classmethod
	def cartesian_product(cls, dimension, carrying_set=None):
		if carrying_set is None:
			carrying_set = allExcept()

		if not isinstance(carrying_set, metaLetter):
			carrying_set = allIn(carrying_set)
		return cls((carrying_set,)*dimension)

	@classmethod
	def all(cls, dimension):
		return cls.cartesian_product(dimension)

	def all_on(self, i):
		"""
		Return the tuple self where the carrying set of dimension i is everything.
		Keeps the constraint.
		"""
		l = list(self._metatuple)
		l[i] = allExcept()
		return self.__class__(tuple(l), equals=self.equals_iter(), not_equals=self.not_equals_iter())

	def equals_iter(self):
		for S in self._uniq_set:
			t = tuple(S)
			yield from [(t[i], t[i+1]) for i in range(len(t)-1)]

	def not_equals_iter(self):
		for i, S in self._not_equals.items():
			if S:
				yield from ((i, j) for  j in S)

	def project(self, i):
		return self._metatuple[i]

	def equals(self, i, j):
		eq = itertools.chain(self.equals_iter(), ((i,j),))
		return self.__class__(self._metatuple, equals=eq, not_equals=self.not_equals_iter())

	def __eq__(self, other):
		return self.issubset(other) and other.issubset(self)

	def not_equals(self, i, j):
		neq = itertools.chain(self.not_equals_iter(), ((i,j),))
		return self.__class__(self._metatuple, equals=self.equals_iter(), not_equals=neq)

	def __contains__(self, t):
		return self.check_tuple(t)

	def check_tuple(self, t):
		assert self.dimension == len(t)
		a = all(t[i] in self.project(i) for i in range(self.dimension))
		if not a:
			return False
		for i, j in self.equals_iter():
			if t[i] != t[j]:
				return False
		for i, j in self.not_equals_iter():
			if t[i] == t[j]:
				return False
		return True

	def issubset(self, other):
		return self.isdisjoint(other.complement())

	def issuperset(self, other):
		return other.issubset(self)

	def isdisjoint(self, other):
		return self.intersection(other).isempty()

	def isempty(self):
		return not all(self._metatuple)

	def __bool__(self):
		return not self.isempty()

	def concatenate(self, other):
		"""
		Return a metaTuple concatenating
		the component of self and other.
		"""
		t = self._metatuple + other._metatuple
		d1 = self.dimension
		shift = lambda e: (e[0]+d1, e[1]+d1)
		eq = self.equals_iter()
		eq = itertool.chain(eq, map(shift, other.equals_iter()))
		neq = self.not_equals_iter()
		neq = itertool.chain(neq, map(shift, other.not_equals_iter()))
		return self.__class__(t, equals=eq, not_equals=neq)

	def intersection(self, *others):
		if len(others) == 0:
			return self
		other = others[0]
		if isinstance(other, metaTuples):
			return other.intersection(self)

		if not isinstance(other, metaTuple):
			other = metaTuple(other)
		others = others[1:]
		if other.dimension != self.dimension:
			raise ValueError("Pointwise intersection of tuples of different dimension is impossible.")
		t = tuple(self._metatuple[i].intersection(other._metatuple[i]) for i in range(self.dimension))
		return self.__class__(	t,
								equals=itertools.chain(self.equals_iter(), other.equals_iter()),
								not_equals=itertools.chain(self.not_equals_iter(), other.not_equals_iter()))

	def intersection_on_dimension(self, metaLetter, dimension):
		"""Return the metatuple where dimension is intersected with metaletter"""
		t = list(self._metatuple)
		t[dimension] = t[dimension].intersection(metaLetter)
		return self.__class__(t, equals=self.equals_iter(), not_equals=self.not_equals_iter())

	def __getitem__(self, i):
		if isinstance(i, int):
			return self.project(i)
		if isinstance(i, slice):
			indices = list(range(self.dimension))[i]
			reverse_indices = { indices[i]:i for i in range(len(indices)) }
			to_keep = set(reverse_indices)
			t = self._metatuple[i]
			eq = map(lambda e:(reverse_indices[e[0]], reverse_indices[e[1]]), filter(lambda e: to_keep.issuperset(e), self.equals_iter()))
			neq = map(lambda e:(reverse_indices[e[0]], reverse_indices[e[1]]), filter(lambda e: to_keep.issuperset(e), self.not_equals_iter()))
			return self.__class__(t, equals_to=eq, not_equals=neq)
		raise KeyError(i)

	def complement(self):
		"""
		Return a metaTuples (union of metaTuple) covering the complement of self.
		"""
		def gen():
			full = self.cartesian_product(self.dimension)
			for i in range(self.dimension):
				yield full.intersection_on_dimension(self[i].complement(), i)
			for i,j in self.equals_iter():
				yield full.not_equals(i, j)
			for i,j in self.not_equals_iter():
				yield full.equals(i, j)
		return metaTuples(*gen())

	def union(self, *others):
		return metaTuples(self, *others)

	def __iter__(self):
		yield self

	def difference(self, other):
		return self.intersection(other.complement())

	def __hash__(self):
		return hash((self._metatuple, tuple(sorted(self.equals_iter())), tuple(sorted(self.not_equals_iter()))))

class metaTuples(metaObject):
	"""
	A class to manipulate finite collections of metatuples.
	Support intersection, union, complement, difference, symetric difference.

	By hypothesis, not necessary a disjoint partition.
	All metaTuple in metaTuples are nonempty (filtered at init).
	"""
	def __init__(self, *metatuples, simplify=True):
		if len(metatuples) == 0:
			raise ValueError("metaTuples cannot be instantiated without any metaTuple")
		_metatuples = tuple(e for e in metatuples if not isinstance(e, metaTuples))
		metatuples = _metatuples + tuple(itertools.chain(*(e._metatuples for e in metatuples if isinstance(e, metaTuples))))
		self.dimension = metatuples[0].dimension
		self._metatuples = tuple(set(filter(bool, (e if isinstance(e, metaTuple) else metaTuple(e) for e in metatuples))))
		if not self._metatuples:
			self._metatuples = (metaTuple.empty_tuple(self.dimension),)
		# Ugly simplification in dimension 1:
		if self.dimension == 1 and simplify:
			x = self._metatuples[0][0]
			x = x.union(*[e[0] for e in self._metatuples[1:]])
			self._metatuples = (metaTuple((x,)),)
		assert all(e.dimension == self.dimension for e in self._metatuples)

	def __contains__(self, t):
		return any(t in e for e in self._metatuples)

	def __bool__(self):
		return all(self._metatuples)

	def union(self, *others):
		return self.__class__(*itertools.chain(self, *others))

	def __hash__(self):
		if len(self._metatuples) == 1:
			return hash(self._metatuples[0])
		return hash(self._metatuples)

	def all_on(self, i):
		return self.__class__(*(e.all_on(i) for e in self))

	def equals(self, i, j):
		"""
		Return the sub-metaTuples of self where
		component i and j must be equals.
		"""
		return self.__class__(e.equals(i, j) for e in self._metatuples)

	def not_equals(self, i, j):
		"""
		Return the sub-metaTuples of self where
		compnent i and j must be different.
		"""
		return self.__class__(e.not_equals(i, j) for e in self._metatuples)

	def isempty(self):
		return all(e.isempty() for e in self._metatuples)

	def issubset(self, other):
		return not self.isdisjoint(other.complement())

	def issuperset(self, other):
		return other.issubset(self)

	def isdisjoint(self, other):
		return bool(self.intersection(other))

	def __eq__(self, other):
		return self.issubset(other) and other.issubset(self)

	def intersection(self, *others):
		if len(others) == 0:
			return self
		other = others[0]
		if not isinstance(other, metaTuples):
			other = metaTuples(other)
		others = others[1:]
		return self.__class__(*(e.intersection(f) for e in self for f in other)).intersection(*others)

	def partition(self):
		"""
		Return a metaTuples equals to self with each metaTuple pairwise
		disjoint.
		"""
		if len(self._metatuples) == 1:
			return self
		first = self._metatuples[0]
		rest = self.__class__(*self._metatuples[1:]).partition()
		p = []
		for r in rest:
			p.append(r.difference(first))
			p.append(r.intersection(first))
			first = first.difference(r)
		p.append(first)
		return self.__class__(*p, simplify=False)

	def project(self, i):
		s = self._metatuples[0]
		return s.project(i).union(*(o.project(i) for o in self._metatuples[1:]))

	def __getitem__(self, i):
		if isinstance(i, int):
			return self.project(i)
		if isinstance(i, slice):
			return self.__class__(*(e[i] for e in self._metatuples))
		raise KeyError(i)

	def complement(self):
		first = self._metatuples[0].complement()
		return first.intersection(*(e.complement() for e in self._metatuples[1:]))

	def difference(self, other):
		return self.intersection(other.complement())

	def symmetric_difference(self, other):
		return self.difference(other).union(other.difference(self))

	def __iter__(self):
		yield from self._metatuples

	def __repr__(self):
		return " ⋃ ".join(map(repr, self))
