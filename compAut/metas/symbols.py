class fset(frozenset):
	def __repr__(self):
		c = ', '.join(map(str, self))
		return f'{{{c}}}'

	def union(self, *args):
		res = frozenset(self)
		for a in args:
			res = res.union(a)
		return fset(res)

class sinkState:
	def __repr__(self):
		return "⊥"

class Marker:
	def __init__(self, S):
		self.rep = S
	def __repr__(self):
		return f"{self.rep}"

class freshState:
	instances = [0]
	def __init__(self):
		self.index = max(self.__class__.instances) + 1
		self.__class__.instances.append(self.index)
	def __repr__(self):
		return f"<UnState {self.index}>"

