from .version import version
import compAut.algebra
import compAut.models
import compAut.logics
import compAut.exceptions
import compAut.compile
import compAut.machines
import compAut.metas
import compAut.encodings
__version__ = version
