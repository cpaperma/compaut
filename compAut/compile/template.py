import subprocess
import re
from collections import abc
import compAut
import datetime
import tempfile
import shutil
import pathlib
import json

class TemplateCompiler:
	"""
	This class aim to simplify the compilation procedure of a full
	standalone machine.
	At __init__, existance of a supported C-compiler is checked.

	So far, only gcc is supported.

	The goal of the TemplateCompiler is to provide common interface to build
	runable state machines.
	The implementation of each state machines is deferred to classes that will inherit from
	TemplateCompiler class.

	The source code produce can be either a standalone program or simple function to call
	in an external source code.

	The function of interest is called crun and is C-function taking:
		* A stream (FILE)
		* Optional directives
		* Optional output pointer

	It will run the machine over the file and either print on stdout the output values (default)
	and store them at the output pointer positions if not NULL (assuming memory allocation was already done).

	Possibly, it is possible to make the python script build a standalone executable for you.
	It requires `gcc` to be installed.
	Executables will have their own help and standard CLI argument parsing.
	The template used by the Python script can be customized.


	A superclass of template machine should at minima have a crun methods.
	eventually a name, a description.
	"""

	templates_dir = (__file__.rsplit("/", maxsplit=1)[0])+"/ctemplate/"
	compile_cmd = "gcc -mavx2 -Ofast"
	name = "RawTemplater"
	descr = "Do nothing, just an intermediate class to produce the standalone program"
	extra_header = ""
	crun = lambda : " /* main execution should be here */"
	dir_desc = ''
	dir_header = ''
	pyreload = lambda self: f"{self.__class__.__module__}.{self.__class__.__name__}()"
	compaut_dir = pathlib.Path(pathlib.Path.home(), ".compaut")
	bin_path = pathlib.Path(compaut_dir, "bin")

	def __init__(self, templates_dir=None, compile_cmd=None):
		self.templates_dir = templates_dir or self.templates_dir
		if compile_cmd is None:
			self.check_gcc()
		else:
			self.compile_cmd = compile_cmd

	def __repr__(self):
		return self.name

	def dumps(self):
		return json.dumps({})

	def check_gcc(self):
		which = subprocess.Popen("which gcc", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		error = which.stderr.read().decode()
		if error:
			raise SystemError("which is not installed. Is it a Linux-like system?")
		path = which.stdout.read().decode().strip()
		if path:
			gcc = subprocess.Popen(f"{path} --version", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		else:
			raise SystemError("gcc is not installed, compilation is no supported.")
		self.gcc_version = gcc.stdout.read().decode().split("\n")[0]

	def sources(self, **kwargs):
		"""
		Produces the source of the standalone compiled machine.
		It call the machine.run() function and the kwargs
		and output a dict like associating
			-main	: a string containing main.c
			-header : a string containing header.c
			-utils	: a string containing utils.c
			-machine: a string containing machine.c

		kwargs can be used to overrides any keyword templates:
			* extra_header: potentially empty string containing headers to incorporate.
			* version: contains the compAut version used to produces those template.
			* name: the name of the machine (e.g. "FiniteAutomaton")
			* descr: a description of the machine instance (e.g. "Compiled automaton of RegExp (ab)*")
			* compile_info: Options recommandation to pass to gcc (e.g. -mavx2 -Ofast).
			* dir_header: Directives headers that the machine can accept (e.g m32)
			* dir_descr: Directives descriptions. (e.g. mNUM number of match before stopping).
			* python_reload_instruction be a string that contains python code to reload a python version of the machine
			* data_dump be a (non humain readable) string representing machine instance.

		"""
		quoted_kwargs = set(["name", "repr", "version", "date", "compile_info", "dir_header", "dir_desc", "python_reload_inst", "data_dump"])
		tdir = self.templates_dir
		d = {}
		d.setdefault("name", self.name)
		d.setdefault("extra_header", self.extra_header)
		d.setdefault("repr", repr(self))
		d.setdefault("version", compAut.__version__)
		d.setdefault("date", datetime.date.today())
		if getattr(self, "gcc_version", False):
			version = f'with {getattr(self, "gcc_version", "")}'
		else:
			version = ""
		d.setdefault("compile_info", f'Compiled with the command `{self.compile_cmd}{version}` the {datetime.date.today()}')
		d.setdefault("dir_header", self.dir_header)
		d.setdefault("dir_desc", self.dir_desc)
		d.setdefault("run", self.crun())
		d.setdefault("data_dump", self.dumps())
		d.setdefault("python_reload_inst", self.pyreload())
		d.update(kwargs)
		solve_template = re.compile('{([a-zA-Z_]*)}')
		def repl(match):
			r = match.groups()[0]
			if r in quoted_kwargs:
				fr = str(d[r]).replace("\n","\\n")
				return f'"{fr}"'
			return d[r]
		sources = {}
		for f in "main", "header", "utils", "machine":
			with open(f"{tdir}{f}.c") as template_f:
				sources[f] = solve_template.sub(repl, template_f.read())
		return sources

	def build_sources(self, comp_dir=None, override=True, **kwargs):
		"""
		Assemble the templates into a ready to be compiled sourced folder
		specified with "comp_dir".
		Default sources folder will be in ~/.compaut/{name} where name is the
		machine name.
		Default will override existing files, change by setting override=False
		"""
		sources = self.sources(**kwargs)
		if not comp_dir:
			comp_dir = pathlib.Path(self.compaut_dir, self.name)
		path = pathlib.Path(comp_dir)
		self._source_dir = path
		path.mkdir(parents=True, exist_ok=override)
		for k, cont in sources.items():
			with open(pathlib.Path(path, f"{k}.c"), "w") as f:
				f.write(cont)

	def compile(self, target_out=None, name=None, comp_dir=None, clean=True, **kwargs):
		"""
		Compile the source to a binary to make it a standalone executable.
		The binary is then copy in the directory "~/.compaut/bin/". Consider adding
		it to your path for convenience.

		By default, the compilation directory is temporary and deleted after compilation.
		For inspecting produced sources, consider using .build_source instead.

		* target_out: the directory where the binary is put.
		* name: the name used for the compilation process. Override other names.
		"""
		if comp_dir is None:
			comp_dir = tempfile.mkdtemp(suffix="_compAut")
		if name is None:
			name = self.name
		try:
			self.build_sources(comp_dir=comp_dir, name=name, **kwargs)
			target = pathlib.Path(comp_dir, "main.c")
			if target_out is None:
				self.bin_path.mkdir(parents=True, exist_ok=True)
				target_out = pathlib.Path(self.bin_path, name)
			self._target_out = target_out
			cmd = f"{self.compile_cmd} {target} -o {target_out}"
			proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
			error = proc.stderr.read().decode()
			if error:
				raise compAut.exceptions.CompileError(error)
		except:
			raise
		finally:
			if clean:
				shutil.rmtree(comp_dir)
	@property
	def target_out(self):
		if "_target_out" in dir(self):
			return self._target_out
		self.bin_path.mkdir(parents=True, exist_ok=True)
		target_out = pathlib.Path(self.bin_path, self.name)
		self._target_out = target_out
		return self._target_out

	def check_if_compiled(self):
		target_out = self.target_out
		if not target_out.is_file():
			raise ValueError("No compiled version found. Call .compile first")

	def clean(self):
		if "source_dir" in dir(self):
			shutil.rmtree(self.source_dir)
		if "_target_out" in dir(self):
			self._target_out.unlink()

	def execute_on_str(self, string):
		return tuple(self.execute_on_iterate((string,)))[0]

	def execute_on_iterate(self, iterator, read_on_write=False):
		"""
		TODO: make this using asyncio
		Execute the machine of iterator.
		If read_on_write is set, it will perform a read after each write.
		Read is blocking so if the machine is not outputing anything.
		"""
		self.check_if_compiled()
		PIPE = subprocess.PIPE
		proc = subprocess.Popen(str(self.target_out), stdin=PIPE, stdout=PIPE)
		for string in iterator:
			proc.stdin.write(string.encode())
			if read_on_write:
				yield proc.stout.read().decode()
		proc.stdin.close()
		yield proc.stdout.read().decode()

	def execute_on_files(self, *files):
		self.check_if_compiled()
		PIPE = subprocess.PIPE
		proc = subprocess.Popen((self._target_out,)+tuple(files), stdout=PIPE)
		return proc.stdout
