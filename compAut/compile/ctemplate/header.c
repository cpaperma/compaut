#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
// Timing stuff
#include <time.h>
#define init_time clock_t t
#define start_time t = clock()
#define get_time (1000*((long)clock() -t))/CLOCKS_PER_SEC
char * time_marker = "";
#define printtime printf("%s:%d ms", time_marker, get_time);
// extra headers will be inserting there during templating.
{extra_header}
/* Local files to include */
#include "machine.c"
#include "utils.c"
/* Global informations */
#define version {version}
#define date {date}
#define compile_info {compile_info}
