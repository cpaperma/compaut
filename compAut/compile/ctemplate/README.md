This folder contains (almost) C files that needs to be formatted
and compiled to produces a standalone executable.

The following files requires formatting:
	* header.c requires:
		* extra_header: potentially empty string containing headers to incorporate.
		* version: contains the compAut version used to produces those template.
	* machine.c
		* name: the name of the machine (e.g. "FiniteAutomaton")
		* repr: a string description of the machine instance (e.g. "Compiled automaton of RegExp (ab)*")
		* compile_info: Options recommandation to pass to gcc (e.g. -mavx2 -Ofast).
		* dir_header: Directives headers that the machine can accept (e.g m32)
		* dir_descr: Directives descriptions. (e.g. mNUM number of match before stopping).
		* python_reload_inst be a string that contains python code to reload a python version of the machine
		* data_dump be a (non humain readable) string representing machine instance.
		* run the code to actually execute.
