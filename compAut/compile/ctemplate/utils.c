/*
	File to display the help of the standalone package.
	* It inherit from compilation directive that should be
	provided in machine.c:
		* name
		* descr should be a string that describe the machine in an human readable way
		* compile_info should contains compilation information
		* dir_header be a string that contains the supported flag to control the run of the mchine
		* dir_desc be a string that contains documentation for the directives
		* python_reload_instruction be a string that contains python code to reload a python version of the machine
		* data_dump be a (non humain readable) string representing machine instance.
*/

void help(){
	printf("Usage: %s [OPTIONS] [FILES]\n\n", name);
	printf("The programs take at least one arguments (path to files). If none is given, it will read from stdin.\n");
	printf("This programs executes %s over the file.\n", repr);
	printf("The output will be printed on stdout.\n\n");
	printf("Options:\n");
	printf("\t-h, --help	display this help\n");
	printf("\t-d FILE, --dump=FILE	print on stdout instructions to obtained the running machine");
	printf(" in the compAut python module and dump to file data requires.\n");
	printf("\t-t, --time	output execution time on stdout in ms.\n");
	printf("\t-x OPTIONS, --directives=OPTIONS provides execution directives%s\n", dir_header);
	printf("\t\t%s\n\n", dir_desc);
	printf("This help has been generated automatically by compAut (version %s).\n", version);
	printf("Compiled with %s.\n", compile_info);
}

void dump(FILE * file, char * path){
	if (path == NULL) fprintf(stderr, "Object is dumped on stdout\n");
	else fprintf(stderr, "Object is dumped in file %s\n", path);
	fprintf(stderr, "%s\n", python_reload_inst);
	fprintf(file, "%s\n", data_dump);
}
