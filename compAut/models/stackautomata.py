from compAut.models.statemachines import RunnableMachine

class StackSymbol:
	"""
	An abstract class to deal with special stack symbols.
	"""
	def __repr__(self):
		return self._repr
	def __eq__(self, other):
		return isinstance(other, self.__class__)
	def __hash__(self):
		return hash(self.__class__)

class EmptyStack(StackSymbol):
	"""
	Symbol representing the empty stack.
	"""
	_repr = "∅"

class CurrentReadLetter(StackSymbol):
	"""
	Symbol to denote the current read letter
	in transitions.
	"""
	_repr = "Current"

class StackOperation:
	_repr = "_"
	def __repr__(self):
		return self._repr

class PushStack(StackOperation):
	_repr = "↥"
	def __init__(self, stack_symbol):
		self.stack_symbol = stack_symbol

	def __repr__(self):
		return f"{self._repr}: {self.stack_symbol}"

class PopStack(StackOperation):
	_repr = "↧"

class Nothing(StackOperation):
	_repr = "_"

class StackAutomaton(RunnableMachine):
	"""
	An implementation of a stack automaton.

	The transitions of a StackAutomaton is a quadruplet:
		state => (letter, stack_letters) => states => Push(StackLetter) or Pop() or Nothing() or None

	(letter, stack_letters) can be a metaTuple. For instance,
 		* metaTuples.all(2) represents a transitions accepting every pairs of symbols.
		* metaTuples.identity(2) represents a transitions accepting only pairs (x, x) for x being any simbols.


	StackLetter can be an explicit symbol, a stack symbols or a metaletters of those.

	A configuration of the StackAutomaton is a Stack and a state.
	When meeting the Push and Pop action, the StackAutomaton update the configuration
	accordingly.

	The stack is simply model by a list using append, lookup to head with [-1] and pop methods.
	"""
	_dimension = 2
	_accept_with_empty_stack = True
	def initial_configurations(self):
		"""
		A configuration is a couple of a list and an initial states.
		"""
		return set(((), i) for i in self.initial_states)

	@property
	def accept_with_empty_stack(self):
		return self._accept_with_empty_stack

	def is_accepting_configuration(self, configuration):
		"""
		A state machine accepts if its configuration state is final.
		Different semantic exists (with empty_stack for instance).
		Overload this method to change the semantic.
		"""
		if self.accept_with_empty_stack:
			return not configuration[0] and configuration[1] in self.final_states
		else:
			return configuration[1] in self.final_states

	def run(self, word):
		configurations = self.initial_configurations()
		for w in word:
			nconfigurations = set()
			for stack, state in configurations:
				if stack:
					head = stack[-1]
				else:
					head = EmptyStack()
				succ = self.get_successors(state, w, head)
				for nstate, Sym in succ:
					nstack = stack
					if isinstance(Sym, PushStack):
						let = Sym.stack_symbol
						if isinstance(let, CurrentReadLetter):
							let = w
						nstack = stack + (let,)
					if isinstance(Sym, PopStack):
						if len(stack) == 0:
							continue
						nstack = stack[:-1]
					nconfigurations.add((nstack, nstate))
			configurations = nconfigurations
		return configurations

	def is_accepted(self, word):
		return any(map(self.is_accepting_configuration, self.run(word)))

	def __contains__(self, word):
		return self.is_accepted(word)


def DyckLanguages(*pairs):
	"""
	Return a stack automaton checking that the input words is well parenthesed.
	Arguments must be pairs (of parenthesis) to match against the input word.
	"""
	from compAut.metas import allIn, allExcept, freshState

	opens = allIn([p[0] for p in pairs])
	closed = allIn([p[1] for p in pairs])
	symbols = opens.union(closed)
	transitions = [(p[0], (p[1], q[0]), q[0], PopStack())  for p in pairs for q in pairs]
	transitions.extend([(p[0], (q[0], allExcept()), q[0] , PushStack(p[0])) for p in pairs for q in pairs])
	transitions.extend([(p[0], (symbols.complement(), allExcept()), p[0]) for p in pairs])
	init = freshState()
	transitions.extend([(p[0], (p[1], EmptyStack()), init) for p in pairs])
	transitions.extend([(init, (p[0], allExcept()), p[0]) for p in pairs])
	transitions.append((init, (symbols.complement(), allExcept()), init))
	return StackAutomaton.from_transitions(transitions, initial_states=[init], final_states=opens.union([init]), accept_with_empty_stack=True)

