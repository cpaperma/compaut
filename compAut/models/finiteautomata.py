import itertools, functools, collections
from compAut.metas import allExcept, allIn, metaLetter, fset, Marker, metaTuple
from compAut import algebra
from compAut.models.statemachines import AbstractStatesMachine, RunnableMachine
from compAut.utils import BinRel

__all__ = ["SemiAutomaton", "Automaton", 'word_to_automaton', 'set_to_automaton']


class SemiAutomaton(AbstractStatesMachine):
	"""
	A SemiAutomaton is an abstract machine without actions.
	Internal representation is left as it is, but get_successors and iter_transitions
	are modified to reflect that change.
	"""
	_dimension = 1
	def get_successors(self, state, letter):
		cache = getattr(self, "_succ_cache", {})
		if not cache:
			self._succ_cache = cache
		if not (state, letter) in cache:
			cache[(state, letter)] = fset(e for e, _ in super().get_successors(state, letter))
		return cache[(state, letter)]

	def get_successors_from(self, states, letter):
		nstates = set()
		for s in states:
			nstates.update(self.get_successors(s, letter))
		return fset(nstates)

	def powerset(self, starting_with=None, **kwargs):
		if not starting_with:
			starting_with = [(s,) for s in self.states]
		NextStates = [fset(start) for start in starting_with]
		transitions = []
		states = set(NextStates)
		while NextStates:
			nstates = NextStates.pop()
			letters = list(l[0] for s in nstates for l in self._transitions.get(s, {}).keys())
			if len(letters) == 0:
				support = (allExcept(),)
			else:
				support = list(letters[0].refine_partition(*letters[1:]))
				support.append(support[0].union(*support[1:]).complement())
			for letter in support:
				newstate = self.get_successors_from(nstates, letter)
				if newstate not in states:
					NextStates.append(newstate)
					states.add(newstate)
				transitions.append((nstates, letter, newstate))
		return self.from_transitions(transitions, self, **kwargs)

	def transition_monoid(self):
		return algebra.TransitionMonoid(self)

	def transition_semigroup(self):
		return algebra.TransitionSemigroup(self)

	def transition_stamp(self):
		return algebra.TransitionStamp(self)


class Automaton(SemiAutomaton, RunnableMachine):
	def powerset(self, **kwargs):
		P = super().powerset(starting_with=[self.initial_states], initial_states=fset((self.initial_states,)), **kwargs)
		P._final_states = fset(p for p in P.states if any(s in self.final_states for s in p))
		return P

	def trim(self, **kwargs):
		trimed_states = set(self.initial_states)
		closure = set(self.initial_states)
		while closure:
			state = closure.pop()
			t = self.get_successors(state, allExcept())
			closure.update(t.difference(trimed_states))
			trimed_states.update(t)
		transitions = filter(lambda e: e[0] in trimed_states and e[2] in trimed_states, self.iter_transitions())
		transitions = list(transitions)
		return self.from_transitions(transitions, self, final_states=self.final_states.intersection(trimed_states), **kwargs)

	def multiple_minimization(self, *sets_of_states):
		"""
		Will produce a minimal automaton recognizing the
		Input:
			* self: should be deterministic or an error is raised.
			* sets_of_states: an enumeration of set of states (colors).
		Output:
			* a (minimal) deterministic Automaton accepting all languages with set_of_states as acceptors.
			* The list of new accepting states in the same order than set_of_states.
		"""
		if not self.is_deterministic():
			raise ValueError("multiple simultaneous minimization requires a deterministic automaton")
		B = self.complete().trim()
		alphabet = B.support_alphabet
		sets_of_states = [S.intersection(B.states) for S in sets_of_states]
		states = allIn(B.states)
		partition = list(state.refine_partition(set_of_states)) # Get disjoint set of states
		D = BinRel(*((i, s) for i, S in enumerate(partition) for s in S))
		# Two element are equivalent if they are in the same sets.
		# i.e. D[i] is a set of equivalent states. We need to refine that in the spirit of Hopecroft algorithm.
		to_deal_with = list(range(len(partition)))
		while to_deal_with:
			i = to_deal_with.pop()
			eq_states = set(D.left[i])
			for a in alphabet:
				R = BinRel() # intermediate BinRel to update or discard.
				for state in eq_states:
					next_state = next(iter(B.get_succesors(state, a)))
					# state reachable by a from state
					next_eq = next(iter(D.right[next_state]))
					# equivalence class of next_state
					R.add(((i, next_eq), state))
				if len(R.left) == 1: # no split of the equivalence class spotted !
					continue
				# otherwise, we remove the previous one
				for state in eq_states:
					D.remove((i, state))
				# and we update
				D.update(R)
				to_deal_with.extend(R.left.keys())
		states = D.left
	def minimize_brzozowski(self):
		return self.reverse().powerset().reverse().powerset().complete().trim().normalize()

	def minimize_hopcroft(self):
		raise NotImplementedError("this should be done soon")

	def minimize(self, algorithm="brzozowski"):
		if algorithm == "hopcroft":
			return self.minimize_hopcroft()
		if algorithm == "brzozowski":
			return self.minimize_brzozowski()
		raise NotImplementedError("Algorithm {algorithm} is not implemented. Try either 'hopcroft' or 'brzozowski'")

	def run(self, word):
		states = self.initial_states
		for w in word:
			states = self.get_successors_from(states, w)
		return fset(states)

	def is_accepted(self, word):
		return bool(self.run(word).intersection(self.final_states))

	def __contains__(self, word):
		return self.is_accepted(word)

	def kleene_star(self):
		transitions = []
		for source, letter, target in self.iter_transitions():
			if target in self.final_states:
				transitions.extend(((source, letter, i) for i in self.initial_states))
			transitions.append((source, letter, target))
		return self.from_transitions(transitions, self, final_states=self.initial_states)

	def complement(self, **kwargs):
		A = self.powerset()
		A._final_states = fset([s for s in A._states if not s.intersection(self.final_states)])
		return A

	def intersection(self, other):
		return self.complement().union(other.complement()).complement().normalize()

	def difference(self, other):
		return self.intersection(other.complement()).normalize()

	def concatenate(self, other, labels=None):
		if labels is None:
			left = Marker("L")
			right = Marker("R")
		else:
			left, right = labels
		transitions = list(((left, source), letter, (left, target)) for (source, letter, target) in self.iter_transitions())
		transitions.extend(((right, source), letter, (right, target)) for (source, letter, target) in other.iter_transitions())
		for (source, letter, target) in self.iter_transitions():
			if target not in self.final_states:
				continue
			for initial in other.initial_states:
				transitions.append(((left, source), letter, (right, initial)))
		initial_states = set((left, s) for s in self.initial_states)
		final_states = set((right, s) for s in other.final_states)
		if self.initial_states.intersection(self.final_states):
			initial_states.update(((right, s) for s in other.initial_states))
		return self.from_transitions(transitions, self, initial_states=initial_states, final_states=final_states)

	def iterate(self, k, accepting=None):
		"""
		Concatenation of self kth by itself.
		accept all indices in accepting or the last one by default.
		"""
		accepting = [k] if accepting is None else accepting
		markers = { i:Marker(str(i)) for i in range(1, k+1) }
		transitions = [[((markers[i], source), letter, (markers[i], target)) for i in range(1, k+1)] for source, letter, target in self.iter_transitions()]
		transitions = sum(transitions, [])
		if self.initial_states.intersection(self.final_states):
			initial_states = {(markers[i], s) for s in self.initial_states for i in range(1, k+1)}
		else:
			initial_states = {(markers[1], s) for s in self.initial_states}
		for (source, letter, target) in self.iter_transitions():
			if target not in self.final_states:
				continue
			for i in range(1, k):
				for initial in self.initial_states:
					transitions.append(((markers[i], source), letter, (markers[i+1], initial)))
		final_states = set(((markers[i], s) for s in self.final_states for i in accepting))
		return self.from_transitions(transitions, initial_states=initial_states, final_states=final_states)

	def is_empty(self):
		if not self.minimize().final_states:
			return True
		return False

	def is_full(self):
		return self.complement().is_empty()

	def is_equivalent_to(self, other):
		return self.difference(other).union(other.difference(self)).is_empty()

	def semilattice_cover(self, alphabet, verbose=False):
		"""
			Return an automaton over "alphabet" alphabet, which is a semilattice
			cover of the input automaton:
			TODO:Describes the construction (is it standard?)

			First we normalize and tabulate to avoid extra cost of automaton construction.
		"""
		import networkx as nx
		G = self.to_networkx_graph()
		C = nx.condensation(G)
		return d, colors

	def to_networkx_graph(self):
		import networkx as nx
		return nx.DiGraph(
			(e, f, {"letter":a[0]})
				for e,v in self._transitions.items()
					for a, d in v.items() for f in d)

	def is_rejecting_sink(self, state):
		x = self.get_successors(state, allExcept())
		if len(x) == 1 and state in x and state not in self.final_states:
			return True
		else:
			return False

	def lookbehind(self, k, alphabet, verbose=True, minimize=True):
		assert self.is_deterministic()
		def alls(i):
			if i == 0:
				return [()]
			A = alls(i-1)
			return A + [ (a,)+u for a in alphabet for u in A]
		initial_state = (next(iter(self.initial_states)), ())
		to_deal_with = [initial_state]
		tr = {}
		i = 0
		while to_deal_with:
			i += 1
			if verbose and i%1000:
				print(f"Remain:{len(to_deal_with)}", end="\r")
			state, local = current = to_deal_with.pop()
			tr[current] = h = {}
			for a in alphabet:
				nlocal = (local+(a,))[-k:]
				nstate = next(iter(self.get_successors(state, allIn((a,)))))
				ncurrent = (nstate, nlocal)
				h[metaTuple(((nlocal,),))] = {ncurrent:None}
				if ncurrent not in tr:
					to_deal_with.append((nstate, nlocal))
		final_states = set(self.final_states)
		final_states = list(filter(lambda e:e[0] in final_states, tr))
		Aut = Automaton.build_from(transitions=tr, initial_states=[initial_state], final_states=final_states)
		if minimize:
			if verbose:
				print(f"Start minimization of a {len(Aut.states)} states automaton")
			Aut = Aut.minimize()
		return Aut




def set_to_automaton(letters):
	"""
	Build an automaton whose transitions belongs to letters.
	+ letters should be at least an iterable.
	"""
	if not isinstance(letters, metaLetter):
		letters = allIn(letters)
	transitions = ((0, letters, 1),)
	return Automaton.from_transitions(transitions, initial_states=[0], final_states=[1])

def word_to_automaton(word):
	"""
	Build an automton recognizing exactly word.
	+ word should be an iterable (sequence).
	"""
	transitions = tuple((i, (a,), i+1) for i,a in enumerate(word))
	transitions += ((len(word), allExcept(), len(word)+1),)
	return Automaton.from_transitions(transitions, initial_states=[0], final_states=[len(word)])

def words_to_automaton(*words, minimize=False):
	transitions = []
	initial = []
	final = []
	states = set()
	for i, word in enumerate(words):
		transitions.extend([((i,j), (l,), (i,j+1)) for j,l in enumerate(word)])
		initial.append((i, 0))
		final.append((i, len(word)))
	A = Automaton.from_transitions(transitions, initial_states=initial, final_states=final)
	if minimize:
		A = A.minimize()
	return A
