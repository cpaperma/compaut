from compAut.models.statemachines import RunnableMachine
from abc import abstractmethod

class ProductionSymbol:
	@abstractmethod
	def __call__(self, letter):
		pass

class SameAsInput(ProductionSymbol):
	def __call__(self, letter):
		return (letter,)

class DirectionSymbol:
	@abstractmethod
	def __call__(self, pos):
		pass
class LeftDirection(DirectionSymbol):
	def __call__(self, pos):
		return pos - 1

class RightDirection(DirectionSymbol):
	def __call__(self, pos):
		return pos + 1

class FiniteTransducers(RunnableMachine):
	_dimension = 1
	def initial_configurations(self):
		return set((i, ()) for i in self.initial_states)

	def run(self, word):
		configurations = self.initial_configurations()
		for w in word:
			nconfigurations = set()
			for state, prod_word in configurations:
				succ = self.get_successors(state, w)
				for state, prod in succ:
					if isinstance(prod, ProductionSymbol):
						prod = prod(w)
					nconfigurations.add((state, prod_word+prod))
			configurations = nconfigurations
		return [word for _, word in filter(lambda e:e[0] in self.final_states, configurations)]

	def deterministic_run(self, word):
		state, _ = next(iter(self.initial_configurations))
		for w in word:
			state, prod = next(iter(self.get_successors(state, w)))
			if isinstance(prod, ProductionSymbol):
				prod = prod(w)
			yield prod


class TwoWayFiniteTransducers(RunnableMachine):
	_dimension = 1
	def run(self, word):
		configurations = {(s, 0, ()) for s in self.initial_states}
		word = iter(word)
		record_word = [next(word)]
		results = []
		while len(configurations) > 0:
			nconfigurations = set()
			r = len(record_word)
			for state, i, prod_word in configurations:
				succ = self.get_successors(state, record_word[i])
				for state, (movement, prod) in succ:
					pos = movement(i)
					if pos >= len(record_word):
						for _ in range(len(record_word), pos+1):
							try:
								record_word.append(next(word))
							except StopIteration:
								continue
					if isinstance(prod, ProductionSymbol):
						prod = prod(record_word[i])
					prod_word += prod
					if state in self.final_states:
						results.append(prod_word)
					if pos>=0:
						nconfigurations.add((state, pos, prod_word))
			configurations = nconfigurations
		return results
