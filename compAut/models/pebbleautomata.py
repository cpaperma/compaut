"""
	An extension of finite automata where some states can have an extra register semantic
	by storing either the first or the last (or both) positions that encounter them during a run.

	The automaton output the content of its register for accepted states.
"""
from .finiteautomata import Automaton

class PebbleAutomaton(Automaton):
	pass
