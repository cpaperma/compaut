# Automata model

* Transducers
* register automatas
* better display of img
* Block automatas
* Tree automata?

# Template

* Template compilation for stack
* Cleaning of the template compilation modes (flags, i/o handling)
* More languages?

# Logics

* more expressive regexps
* basic Xpath for XML/JSON
* basic Jpath for Trees

# Benchmarking tools

* Everything

# Algebra

* better visualization of egg box
* Forest algebra for trees?
